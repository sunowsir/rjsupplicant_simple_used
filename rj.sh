#!/bin/bash


path=$(cd $(dirname "$0") && pwd)

cd $path

if [[ ! -f ./data/usr.info ]];then
    touch ./data/usr.info
fi
if [[ ! -f ./data/output.info ]];then
    touch ./data/output.info
fi

case $1 in
    "run")

        lastid=`tail -1 ./data/usr.info | cut -d '|' -f1`
        lastpwd=`tail -1 ./data/usr.info | cut -d '|' -f2`
        last_usrpwd=`tail -1 ./data/usr.info | cut -d '|' -f3`
        last_netname=`tail -1 ./data/usr.info | cut -d '|' -f4`
        judge="no"
        
        if [[ "x"${last_usrpwd} != "x" ]];then
            userpwd=${last_usrpwd}
        else
            # gksu -p -D "锐捷校园网客户端" 2> /dev/null | read userpwd
            echo -n "input passwd : "
            read -s userpwd
            echo
            if [[ "x${userpwd}" == "x" ]];
            then
                exit 1
            fi
        fi

        if [[ "x"${lastid} != "x" ]];then
            echo -n "是否使用上次登录账号 <${lastid}> (yes/no)?"
            read judge
        fi
        if [[ ${judge} == "yes" || ${judge} == "y" || ${judge} == "Y" || ${judge} == "YES" ]];then
            id=${lastid}
            passwd=${lastpwd}
        else 
            echo -n "校园网账号:"
            read id
            echo -n "密码:"
            read -s passwd
            echo
        fi

        if [[ "x"${last_netname} != "x" ]];then
            netname=${last_netname}
        else
            echo -n "输入有线网卡名称："
            read netname
        fi

        echo "${id}|${passwd}|${userpwd}|${netname}" > ./data/usr.info

        echo ${userpwd} | sudo -S bash ./src/rjsupplicant.sh -u ${id} -p ${passwd} -d 0 -a 1 -n ${netname} 1> ./data/output.info 2> /dev/null &

        echo "Please wait..."

        outline=-1

        second1=`date +"%S"`

        while true
        do

            second2=`date +"%S"`
            runtimes=`echo "${second2} ${second1}" | awk '{printf("%d", $2 - $1)}'`
            if [[ ${runtimes} -ge 10 ]];then
                break;
            fi

            lastline=${outline}
            outline=`tail -1 ./data/output.info | cut -d ' ' -f 3`
            if [[ ${outline} == "认证成功" || ${outline} == "客户端已经在运行。" ]];then
                echo `tail -1 ./data/output.info`
                break;
            fi
            if [[ ${outline} == ${lastline} ]];then
                continue;
            fi
            echo `tail -1 ./data/output.info`

        done

        echo ${userpwd} | sudo -S service network-manager restart > /dev/null 2>&1

        echo `date +"%Y-%m-%d %H:%M:%S"`" 正在重启系统(Network-Manager)服务"

    ;;
    "quit")
        
        last_usrpwd=`tail -1 ./data/usr.info | cut -d '|' -f3`

        if [[ "x"${last_usrpwd} != "x" ]];then
            userpwd=${last_usrpwd}
        else
            echo -n "input passwd : "
            read -s userpwd
            echo 
            if [[ "x${userpwd}" == "x" ]];
            then
                exit 1
            fi
        fi

        echo ${userpwd} |sudo -S bash ./src/rjsupplicant.sh -q 1> ./data/output.info 2> /dev/null &

        echo "Please wait..."

        outline=-1

        second1=`date +"%S"`

        while true
        do

            second2=`date +"%S"`
            runtimes=`echo "${second2} ${second1}" | awk '{printf("%d", $2 - $1)}'`
            if [[ ${runtimes} -ge 4 ]];then
                break;
            fi

            lastline=${outline}
            outline=`tail -1 ./data/output.info | cut -d ' ' -f 3`

            if [[ ${outline} == "退出客户端成功" || ${outline} == "客户端未运行" ]];then
                echo `date +"%Y-%m-%d %H:%M:%S"`" "`tail -1 ./data/output.info`
                break;
            fi
            if [[ ${outline} == ${lastline} ]];then
                continue;
            fi
            echo `tail -1 ./data/output.info`

        done

        echo ${userpwd} | sudo -S service network-manager restart > /dev/null 2>&1
        echo `date +"%Y-%m-%d %H:%M:%S"`" 正在重启系统(Network-Manager)服务"


    ;;
    "help")
        echo "参数："
        echo "    校园网客户端认证：run"
        echo "    校园网客户端退出：quit"
        echo "    上述参数无需加 - 或 --";;
    *)
        echo "错误的参数: " $1
        echo "获取更多帮助请输入: rj help";;
esac
